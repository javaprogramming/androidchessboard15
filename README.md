**# Chess Android Project #**
Group 15
Group member: Dingren Nie, Fulai Xing

### This is a chess game android project. ###
### The three main functionalities are: play a game, record the game and reply the game. ###

### After you run the app, a main screen will be showed to you. There are two button, PLAY A NEW GAME and REPLAY A GAME. ### 
## PLAY A NEW GAME: ##
    ###  It will go to a new window to play the game. The recording functionality was integrated within PLAY A NEW GAME, after a game is ended, it will prompt user to enter a name for this game, and choose save or not. ### 
    ### Buttons: ###
        ### Undo: allow user to undo 1 time, after click it, the button will turn to grey. Usable after opponent finish his move. ###
        ### Draw: draw the  game. after click it will ask to save the game. ###
        ### Resign: opponent wins.after click it will ask to save the game. ###
        ### AI: It will choose a move for the current player. Choosing randomly from the set of legal moves is sufficient. ### 
## REPLAY A GAME: ##
    ### It will go to a new window to show a list of saved games. You can delete all saved games for more storage. And you can see the list by date or by title. ###
    ###  Buttons: ###
        ### CLEARALL: Delete all saved games ###
        ### BYDATE: sort the list by date. ###
        ### BYTITLE: Sort the list by title. ###
    ### After you click a saved game, it will navigate to a new windows that show a chessboard with a next button. When you click next button, the chessboard will show the next move## of this recorded game. After end, it will show you "Replay is done". ###