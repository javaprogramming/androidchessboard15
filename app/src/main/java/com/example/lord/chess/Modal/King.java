package com.example.lord.chess.Modal;

import java.util.ArrayList;
import java.util.List;

import com.example.lord.chess.Modal.ChessType.Type;
import com.example.lord.chess.R;

public class King extends Piece {
    public King(Player player, Position position, Board board) {
        this.player = player;
        this.position = position;
        this.board = board;
        this.type = Type.KING;
        if(player.isWhite)
        {
            icon = R.drawable.crown;
        }
        else
        {
            icon = R.drawable.crownfilled;
        }
    }

    private boolean hasMoved = false;

    public boolean isMoved() {
        return hasMoved;
    }

    @Override
    public List<Position> getRange(Board board) {
        // TODO Auto-generated method stub
        //first of all, we find the basic ranges
        //it is same for both white and black
        int file = this.position.getFile();
        int rank = this.position.getRank();
        List<Position> result = new ArrayList<>();
        if (isValidPosition(file - 1, rank - 1, this.isWhiteColor())) {
            result.add(new Position(file - 1, rank - 1));
        }
        if (isValidPosition(file, rank - 1, this.isWhiteColor())) {
            result.add(new Position(file, rank - 1));
        }
        if (isValidPosition(file + 1, rank - 1, this.isWhiteColor())) {
            result.add(new Position(file + 1, rank - 1));
        }
        if (isValidPosition(file - 1, rank, this.isWhiteColor())) {
            result.add(new Position(file - 1, rank));
        }
        if (isValidPosition(file + 1, rank, this.isWhiteColor())) {
            result.add(new Position(file + 1, rank));
        }
        if (isValidPosition(file - 1, rank + 1, this.isWhiteColor())) {
            result.add(new Position(file - 1, rank + 1));
        }
        if (isValidPosition(file, rank + 1, this.isWhiteColor())) {
            result.add(new Position(file, rank + 1));
        }
        if (isValidPosition(file + 1, rank + 1, this.isWhiteColor())) {
            result.add(new Position(file + 1, rank + 1));
        }
        //now check for castling
        if (!this.hasMoved)//king has not moved
        {
            //two rook worth checking
            Piece p1 = this.board.getPosition(0, rank);
            Piece p2 = this.board.getPosition(7, rank);
            Rook r1, r2;
            if (p1 != null) {
                if (p1.type == Type.ROOK) {
                    r1 = (Rook) p1;
                    if (!r1.isMoved()) {
                        boolean notBlocked = (this.board.getPosition(1, rank) == null) &&
                                (this.board.getPosition(2, rank) == null) &&
                                (this.board.getPosition(3, rank) == null);
                        if (notBlocked) {
                            result.add(new Position(2, rank));
                        }
                    }
                }
            }
            if (p2 != null) {
                if (p2.type == Type.ROOK) {
                    r2 = (Rook) p2;
                    if (!r2.isMoved()) {
                        boolean notBlocked = (this.board.getPosition(5, rank) == null) &&
                                (this.board.getPosition(6, rank) == null);
                        if (notBlocked) {
                            result.add(new Position(6, rank));
                        }
                    }
                }
            }

        }
        return result;
    }

    public boolean isChecked() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Piece tmp = board.getPosition(i, j);
                if (tmp == null)//empty
                {
                    continue;
                }
                if (tmp.isWhiteColor() == this.isWhiteColor())//same color
                {
                    continue;
                } else {
                    List<Position> attackRange = tmp.getRange(this.board);
                    if (attackRange == null) {
                        continue;
                    }
                    for (Position position : attackRange) {
                        if (position.equals(this.position)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override
    public boolean validateAndMove(Position oldP, Position newP) {
        // TODO Auto-generated method stub
        //split into two case, castling and non-castling
        //first lets go with castling
        if (oldP.getRank() == newP.getRank() && oldP.getDistance(newP) == 2) {
            Piece mover = this;
            Piece destination = this.board.getPosition(newP);//supposed to be null
            if (mover.getRange(this.board) != null && mover.getRange(this.board).contains(newP)) {
                if (!this.isChecked())//can only happen if king is not checked before the move
                {
                    //split to two cases (new Position on right side, new Position on left side)
                    if (newP.getFile() == 6)//file is 6, the rook is index 7
                    {
                        Rook rook = (Rook) this.board.getPosition(new Position(7, oldP.getRank()));
                        //first try to move king a step
                        this.board.setPosition(new Position(5, oldP.getRank()), this);
                        this.board.setPosition(oldP, null);
                        if (this.isWhiteColor()) {
                            this.board.whiteKing = new Position(5, oldP.getRank());
                        } else {
                            this.board.blackKing = new Position(5, oldP.getRank());
                        }
                        if (this.isChecked())//revert
                        {
                            this.board.setPosition(oldP, this);
                            this.board.setPosition(new Position(5, oldP.getRank()), null);
                            if (this.isWhiteColor()) {
                                this.board.whiteKing = oldP;
                            } else {
                                this.board.blackKing = oldP;
                            }
                            return false;
                        }
                        //try the next move
                        this.board.setPosition(new Position(6, oldP.getRank()), this);
                        this.board.setPosition(new Position(5, oldP.getRank()), rook);
                        this.board.setPosition(new Position(7, oldP.getRank()), null);
                        if (this.isWhiteColor()) {
                            this.board.whiteKing = new Position(6, oldP.getRank());
                        } else {
                            this.board.blackKing = new Position(6, oldP.getRank());
                        }
                        if (this.isChecked()) {
                            this.board.setPosition(oldP, this);
                            this.board.setPosition(new Position(7, oldP.getRank()), rook);
                            this.board.setPosition(new Position(6, oldP.getRank()), null);
                            this.board.setPosition(new Position(5, oldP.getRank()), null);
                            if (this.isWhiteColor()) {
                                this.board.whiteKing = oldP;
                            } else {
                                this.board.blackKing = oldP;
                            }
                            return false;
                        }
                        //succeeded, set both to moved
                        this.hasMoved = true;
                        rook.setMoved();
                        return true;


                    } else//file is 2, the rook is the index 0
                    {
                        Rook rook = (Rook) this.board.getPosition(new Position(0, oldP.getRank()));
                        //first try to move king a step
                        this.board.setPosition(new Position(3, oldP.getRank()), this);
                        this.board.setPosition(oldP, null);
                        if (this.isWhiteColor()) {
                            this.board.whiteKing = new Position(3, oldP.getRank());
                        } else {
                            this.board.blackKing = new Position(3, oldP.getRank());
                        }
                        if (this.isChecked())//revert
                        {
                            this.board.setPosition(oldP, this);
                            this.board.setPosition(new Position(3, oldP.getRank()), null);
                            if (this.isWhiteColor()) {
                                this.board.whiteKing = oldP;
                            } else {
                                this.board.blackKing = oldP;
                            }
                            return false;
                        }
                        //try the next move
                        this.board.setPosition(new Position(2, oldP.getRank()), this);
                        this.board.setPosition(new Position(3, oldP.getRank()), rook);
                        this.board.setPosition(new Position(0, oldP.getRank()), null);
                        if (this.isWhiteColor()) {
                            this.board.whiteKing = new Position(2, oldP.getRank());
                        } else {
                            this.board.blackKing = new Position(2, oldP.getRank());
                        }
                        if (this.isChecked()) {
                            this.board.setPosition(oldP, this);
                            this.board.setPosition(new Position(0, oldP.getRank()), rook);
                            this.board.setPosition(new Position(2, oldP.getRank()), null);
                            this.board.setPosition(new Position(3, oldP.getRank()), null);
                            if (this.isWhiteColor()) {
                                this.board.whiteKing = oldP;
                            } else {
                                this.board.blackKing = oldP;
                            }
                            return false;
                        }
                        //succeeded, set both to moved
                        this.hasMoved = true;
                        rook.setMoved();
                        return true;
                    }
                } else//checked before validate
                {
                    return false;
                }
            } else//not in range
            {
                return false;
            }
        } else {
            Piece mightCaptured = this.board.getPosition(newP);
            Piece mover = this;
            if (mover.getRange(this.board) != null && mover.getRange(this.board).contains(newP)) {//simulate the move
                this.board.setPosition(newP, this);
                this.board.setPosition(oldP, null);
                if (this.isWhiteColor()) {
                    this.board.whiteKing = newP;
                } else {
                    this.board.blackKing = newP;
                }
                if (this.isChecked()) {//revert the move
                    this.board.setPosition(newP, mightCaptured);
                    this.board.setPosition(oldP, this);
                    if (this.isWhiteColor()) {
                        this.board.whiteKing = oldP;
                    } else {
                        this.board.blackKing = oldP;
                    }
                    return false;
                }
                //other wise, clean up mightCaptured
                this.hasMoved = true;
                mightCaptured = null;
                return true;
            }
            return false;
        }
    }

    @Override
    protected boolean validateOnly(Position oldP, Position newP) {
        // TODO Auto-generated method stub
        //split into two case, castling and non-castling
        //first lets go with castling
        if (oldP.getRank() == newP.getRank() && oldP.getDistance(newP) == 2) {
            Piece mover = this;
            Piece destination = this.board.getPosition(newP);//supposed to be null
            if (mover.getRange(this.board) != null && mover.getRange(this.board).contains(newP)) {
                if (!this.isChecked())//can only happen if king is not checked before the move
                {
                    //split to two cases (new Position on right side, new Position on left side)
                    if (newP.getFile() == 6)//file is 6, the rook is index 7
                    {
                        Rook rook = (Rook) this.board.getPosition(new Position(7, oldP.getRank()));
                        //first try to move king a step
                        this.board.setPosition(new Position(5, oldP.getRank()), this);
                        this.board.setPosition(oldP, null);
                        if (this.isWhiteColor()) {
                            this.board.whiteKing = new Position(5, oldP.getRank());
                        } else {
                            this.board.blackKing = new Position(5, oldP.getRank());
                        }
                        if (this.isChecked())//revert
                        {
                            this.board.setPosition(oldP, this);
                            this.board.setPosition(new Position(5, oldP.getRank()), null);
                            if (this.isWhiteColor()) {
                                this.board.whiteKing = oldP;
                            } else {
                                this.board.blackKing = oldP;
                            }
                            return false;
                        }
                        //try the next move
                        this.board.setPosition(new Position(6, oldP.getRank()), this);
                        this.board.setPosition(new Position(5, oldP.getRank()), rook);
                        this.board.setPosition(new Position(7, oldP.getRank()), null);
                        if (this.isWhiteColor()) {
                            this.board.whiteKing = new Position(6, oldP.getRank());
                        } else {
                            this.board.blackKing = new Position(6, oldP.getRank());
                        }
                        if (this.isChecked()) {
                            this.board.setPosition(oldP, this);
                            this.board.setPosition(new Position(7, oldP.getRank()), rook);
                            this.board.setPosition(new Position(6, oldP.getRank()), null);
                            this.board.setPosition(new Position(5, oldP.getRank()), null);
                            if (this.isWhiteColor()) {
                                this.board.whiteKing = oldP;
                            } else {
                                this.board.blackKing = oldP;
                            }
                            return false;
                        }
                        //succeeded, set both to moved
                        this.board.setPosition(oldP, this);
                        this.board.setPosition(new Position(7, oldP.getRank()), rook);
                        this.board.setPosition(new Position(6, oldP.getRank()), null);
                        this.board.setPosition(new Position(5, oldP.getRank()), null);
                        if (this.isWhiteColor()) {
                            this.board.whiteKing = oldP;
                        } else {
                            this.board.blackKing = oldP;
                        }
                        return true;


                    } else//file is 2, the rook is the index 0
                    {
                        Rook rook = (Rook) this.board.getPosition(new Position(0, oldP.getRank()));
                        //first try to move king a step
                        this.board.setPosition(new Position(3, oldP.getRank()), this);
                        this.board.setPosition(oldP, null);
                        if (this.isWhiteColor()) {
                            this.board.whiteKing = new Position(3, oldP.getRank());
                        } else {
                            this.board.blackKing = new Position(3, oldP.getRank());
                        }
                        if (this.isChecked())//revert
                        {
                            this.board.setPosition(oldP, this);
                            this.board.setPosition(new Position(3, oldP.getRank()), null);
                            if (this.isWhiteColor()) {
                                this.board.whiteKing = oldP;
                            } else {
                                this.board.blackKing = oldP;
                            }

                            return false;
                        }
                        //try the next move
                        this.board.setPosition(new Position(2, oldP.getRank()), this);
                        this.board.setPosition(new Position(3, oldP.getRank()), rook);
                        this.board.setPosition(new Position(0, oldP.getRank()), null);
                        if (this.isWhiteColor()) {
                            this.board.whiteKing = new Position(2, oldP.getRank());
                        } else {
                            this.board.blackKing = new Position(2, oldP.getRank());
                        }
                        if (this.isChecked()) {
                            this.board.setPosition(oldP, this);
                            this.board.setPosition(new Position(0, oldP.getRank()), rook);
                            this.board.setPosition(new Position(2, oldP.getRank()), null);
                            this.board.setPosition(new Position(3, oldP.getRank()), null);
                            if (this.isWhiteColor()) {
                                this.board.whiteKing = oldP;
                            } else {
                                this.board.blackKing = oldP;
                            }
                            return false;
                        }
                        //succeeded, set both to moved
                        this.board.setPosition(oldP, this);
                        this.board.setPosition(new Position(0, oldP.getRank()), rook);
                        this.board.setPosition(new Position(2, oldP.getRank()), null);
                        this.board.setPosition(new Position(3, oldP.getRank()), null);
                        if (this.isWhiteColor()) {
                            this.board.whiteKing = oldP;
                        } else {
                            this.board.blackKing = oldP;
                        }
                        return true;
                    }
                } else//checked before validate
                {
                    return false;
                }
            } else//not in range
            {
                return false;
            }
        } else {
            Piece mightCaptured = this.board.getPosition(newP);
            Piece mover = this;
            if (mover.getRange(this.board) != null && mover.getRange(this.board).contains(newP)) {//simulate the move
                this.board.setPosition(newP, this);
                this.board.setPosition(oldP, null);
                if (this.isWhiteColor()) {
                    this.board.whiteKing = newP;
                } else {
                    this.board.blackKing = newP;
                }
                if (this.isChecked()) {//revert the move
                    this.board.setPosition(newP, mightCaptured);
                    this.board.setPosition(oldP, this);
                    if (this.isWhiteColor()) {
                        this.board.whiteKing = oldP;
                    } else {
                        this.board.blackKing = oldP;
                    }
                    return false;
                }
                //other wise, clean up mightCaptured
                this.board.setPosition(newP, mightCaptured);
                this.board.setPosition(oldP, this);
                if (this.isWhiteColor()) {
                    this.board.whiteKing = oldP;
                } else {
                    this.board.blackKing = oldP;
                }
                return true;
            }
            return false;
        }
    }
}
