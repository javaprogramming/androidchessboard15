package com.example.lord.chess.Modal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by lord on 12/14/16.
 */

public class SaveGame implements Serializable{
    public ArrayList<SingleGame> games;
    public SaveGame (ArrayList<SingleGame> game)
    {
        this.games = game;
    }
    public SaveGame()
    {
        this.games = new ArrayList<SingleGame>();
    }
    public void add(SingleGame s)
    {
        this.games.add(s);
    }

}
