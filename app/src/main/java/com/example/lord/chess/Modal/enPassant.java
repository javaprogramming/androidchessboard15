package com.example.lord.chess.Modal;

import java.io.Serializable;

/**
 * @author lord
 */
//Store the mostRecent position, and the tombstone position
public class enPassant implements Serializable {
    Position mostRecent;
    Position tombstone;//where it might die

    public enPassant(Position mostRecent, Position tombstone) {
        this.mostRecent = mostRecent;
        this.tombstone = tombstone;
    }
}
