package com.example.lord.chess.Modal;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.io.Serializable;
/**
 * Created by lord on 12/14/16.
 */

public class SingleGame implements Serializable{
    public Date date;
    public ArrayList<Move> move;
    public String title;
    public int result;//0 for white win, 1 for black win, 2 for tie
    public SingleGame(Date date, ArrayList<Move> move,String title)
    {
        this.date = date;
        this.move = move;
        this.title = title;
    }

    public String getTitle(){
        return title;
    }
    public Date getDate(){
        return date;
    }
    public ArrayList<Move> getMoves(){
        return move;
    }

    @Override
    public String toString() {
        return "Title: "+ this.title+" Date: "+date;
    }
}
