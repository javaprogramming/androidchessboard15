package com.example.lord.chess.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.Button;

import com.example.lord.chess.R;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void playgame(View view)
    {
        Intent intent = new Intent(this, PlayGame.class);
        startActivity(intent);
    }
    public void replay(View view) {
        Intent intent = new Intent(this, Replay.class);
        startActivity(intent);
    }

}
