package com.example.lord.chess.Modal;

import java.util.ArrayList;
import java.util.List;

import com.example.lord.chess.Modal.ChessType.Type;
import com.example.lord.chess.R;

public class Queen extends Piece {
    public Queen(Player player, Position position, Board board) {
        this.player = player;
        this.position = position;
        this.board = board;
        this.type = Type.QUEEN;
        if(player.isWhite)
        {
            icon = R.drawable.queen;
        }
        else
        {
            icon = R.drawable.queenfilled;
        }
    }

    @Override
    public List<Position> getRange(Board board) {
        List<Position> result = new ArrayList<>();
        int file = this.position.getFile();
        int rank = this.position.getRank();
        for (int i = file - 1; i >= -1; i--) {
            if (isValidPosition(i, rank, this.isWhiteColor())) {
                result.add(new Position(i, rank));
                if (this.board.getPosition(new Position(i, rank)) != null) {
                    break;
                }
            } else {
                break;
            }
        }
        for (int j = file + 1; j <= 8; j++) {
            if (isValidPosition(j, rank, this.isWhiteColor())) {
                result.add(new Position(j, rank));
                if (this.board.getPosition(new Position(j, rank)) != null) {
                    break;
                }
            } else {
                break;
            }
        }
        for (int k = rank - 1; k >= -1; k--) {
            if (isValidPosition(file, k, this.isWhiteColor())) {
                result.add(new Position(file, k));
                if (this.board.getPosition(new Position(file, k)) != null) {
                    break;
                }
            } else {
                break;
            }
        }
        for (int l = rank + 1; l <= 8; l++) {
            if (isValidPosition(file, l, this.isWhiteColor())) {
                result.add(new Position(file, l));
                if (this.board.getPosition(new Position(file, l)) != null) {
                    break;
                }
            } else {
                break;
            }
        }
        //file
        int f = 0;
        //rank
        int r = 0;
        for (f = file - 1, r = rank - 1; f >= -1; f--, r--) {
            if (isValidPosition(f, r, this.isWhiteColor())) {
                result.add(new Position(f, r));
                if (this.board.getPosition(new Position(f, r)) != null) {
                    break;
                }
            } else {
                break;
            }
        }
        for (f = file - 1, r = rank + 1; f >= -1; f--, r++) {
            if (isValidPosition(f, r, this.isWhiteColor())) {
                result.add(new Position(f, r));
                if (this.board.getPosition(new Position(f, r)) != null) {
                    break;
                }
            } else {
                break;
            }
        }
        for (f = file + 1, r = rank + 1; f <= 8; f++, r++) {
            if (isValidPosition(f, r, this.isWhiteColor())) {
                result.add(new Position(f, r));
                if (this.board.getPosition(new Position(f, r)) != null) {
                    break;
                }
            } else {
                break;
            }
        }
        for (f = file + 1, r = rank - 1; f <= 8; f++, r--) {
            if (isValidPosition(f, r, this.isWhiteColor())) {
                result.add(new Position(f, r));
                if (this.board.getPosition(new Position(f, r)) != null) {
                    break;
                }
            } else {
                break;
            }
        }
        return result;
    }
}
