package com.example.lord.chess.Modal;

import java.util.ArrayList;
import java.util.List;

import com.example.lord.chess.Modal.ChessType.Type;
import com.example.lord.chess.R;

public class Bishop extends Piece {
    public Bishop(Player player, Position position, Board board) {
        this.player = player;
        this.position = position;
        this.board = board;
        this.type = Type.BISHOP;
        if(player.isWhite)
        {
            icon = R.drawable.bishop;
        }
        else
        {
            icon = R.drawable.bishopfilled;
        }
    }

    @Override
    public List<Position> getRange(Board board) {
        List<Position> result = new ArrayList<>();
        int file = this.position.getFile();
        int rank = this.position.getRank();
        //file
        int f = 0;
        //rank
        int r = 0;
        for (f = file - 1, r = rank - 1; f >= -1; f--, r--) {
            if (isValidPosition(f, r, this.isWhiteColor())) {
                result.add(new Position(f, r));
                if (this.board.getPosition(new Position(f, r)) != null) {
                    break;
                }
            } else {
                break;
            }
        }
        for (f = file - 1, r = rank + 1; f >= -1; f--, r++) {
            if (isValidPosition(f, r, this.isWhiteColor())) {
                result.add(new Position(f, r));
                if (this.board.getPosition(new Position(f, r)) != null) {
                    break;
                }
            } else {
                break;
            }
        }
        for (f = file + 1, r = rank + 1; f <= 8; f++, r++) {
            if (isValidPosition(f, r, this.isWhiteColor())) {
                result.add(new Position(f, r));
                if (this.board.getPosition(new Position(f, r)) != null) {
                    break;
                }
            } else {
                break;
            }
        }
        for (f = file + 1, r = rank - 1; f <= 8; f++, r--) {
            if (isValidPosition(f, r, this.isWhiteColor())) {
                result.add(new Position(f, r));
                if (this.board.getPosition(new Position(f, r)) != null) {
                    break;
                }
            } else {
                break;
            }
        }
        return result;
    }
}
