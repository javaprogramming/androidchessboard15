package com.example.lord.chess.Modal;

import java.util.ArrayList;
import java.util.List;

import com.example.lord.chess.Modal.ChessType.Type;
import com.example.lord.chess.R;

public class Rook extends Piece {
    public Rook(Player player, Position position, Board board) {
        this.player = player;
        this.position = position;
        this.board = board;
        this.type = Type.ROOK;
        if(player.isWhite)
        {
            icon = R.drawable.rook;
        }
        else
        {
            icon = R.drawable.rookfilled;
        }
    }

    private boolean hasMoved = false;

    public boolean isMoved() {
        return hasMoved;
    }

    public void setMoved() {
        this.hasMoved = true;
    }

    @Override
    public List<Position> getRange(Board board) {
        // TODO Auto-generated method stub
        List<Position> result = new ArrayList<>();
        int file = this.position.getFile();
        int rank = this.position.getRank();
        //there will be fourteen possible positions that we can move to,
        //and should be checked one by another
        //  ---note I made it (rank,file)
        for (int i = file - 1; i >= -1; i--) {
            if (isValidPosition(i, rank, this.isWhiteColor())) {
                result.add(new Position(i, rank));
                if (this.board.getPosition(new Position(i, rank)) != null) {
                    break;
                }
            } else {
                break;
            }
        }
        for (int j = file + 1; j <= 8; j++) {
            if (isValidPosition(j, rank, this.isWhiteColor())) {
                result.add(new Position(j, rank));
                if (this.board.getPosition(new Position(j, rank)) != null) {
                    break;
                }
            } else {
                break;
            }
        }
        for (int k = rank - 1; k >= -1; k--) {
            if (isValidPosition(file, k, this.isWhiteColor())) {
                result.add(new Position(file, k));
                if (this.board.getPosition(new Position(file, k)) != null) {
                    break;
                }
            } else {
                break;
            }
        }
        for (int l = rank + 1; l <= 8; l++) {
            if (isValidPosition(file, l, this.isWhiteColor())) {
                result.add(new Position(file, l));
                if (this.board.getPosition(new Position(file, l)) != null) {
                    break;
                }
            } else {
                break;
            }
        }
        return result;
    }

    @Override
    public boolean validateAndMove(Position oldP, Position newP) {
        // TODO Auto-generated method stub
        King myKing;
        if (this.isWhiteColor()) {
            myKing = (King) this.board.getPosition(this.board.whiteKing);
        } else {
            myKing = (King) this.board.getPosition(this.board.blackKing);
        }
        Piece mightCaptured = this.board.getPosition(newP);
        Piece mover = this;
        if (mover.getRange(this.board) != null && mover.getRange(this.board).contains(newP)) {//simulate the move
            this.board.setPosition(newP, mover);
            this.board.setPosition(oldP, null);
            if (myKing.isChecked()) {//revert the move
                this.board.setPosition(newP, mightCaptured);
                this.board.setPosition(oldP, mover);
                return false;
            }
            //other wise, clean up mightCaptured
            mightCaptured = null;
            setMoved();
            return true;
        }
        //cannot go to the new position, then nothing has been changed, simply return false
        return false;
    }
}
