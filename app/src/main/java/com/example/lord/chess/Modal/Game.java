package com.example.lord.chess.Modal;

import android.view.View;

import java.io.Serializable;
import java.util.List;
import java.util.Scanner;

import com.example.lord.chess.Modal.ChessType.Type;

/**
 * @author lord
 */
//every round, want to Draw will be reinitialized for the player who is playing
public class Game implements Serializable {
    public int turn;
    public Player[] players;
    public Board board;

    public Game() {
        this.turn = 0;
        this.players = new Player[2];
        players[0] = new Player(true);
        players[1] = new Player(false);
        this.board = new Board(players);
    }

    public int getTurn() {
        return turn;
    }

    public boolean isCheckMate(int turn) {
        King king;
        if (turn == 0) {
            king = (King) board.getPosition(board.whiteKing);
        } else {
            king = (King) board.getPosition(board.blackKing);
        }
        return !isNotDeadGame(king);
    }

    public boolean isStaleMate(int turn) {
        King king;
        if (turn == 0) {
            king = (King) board.getPosition(board.whiteKing);
        } else {
            king = (King) board.getPosition(board.blackKing);
        }
        return !isNotDeadGame(king);
    }

    public boolean isNotDeadGame(King king)//check if there is any valid operation that this player can do
    {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Piece current = board.table[i][j];
                if (current == null) {
                    continue;
                } else if (current.isWhiteColor() != king.isWhiteColor())//not players piece
                {
                    continue;
                } else {
                    List<Position> possibleMoves = current.getRange(this.board);
                    if (possibleMoves == null) {
                        continue;
                    } else {
                        for (Position position : possibleMoves) {
                            if (current.validateOnly(current.position, position)) {
                                return true;
                            }
                        }
                    }
                }

            }

        }
        return false;
    }
    public Move AIMove(King king)//check if there is any valid operation that this player can do
    {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Piece current = board.table[i][j];
                if (current == null) {
                    continue;
                } else if (current.isWhiteColor() != king.isWhiteColor())//not players piece
                {
                    continue;
                } else {
                    List<Position> possibleMoves = current.getRange(this.board);
                    if (possibleMoves == null) {
                        continue;
                    } else {
                        for (Position position : possibleMoves) {
                            if (current.validateOnly(current.position, position)) {
                                return new Move(current.position,position);
                            }
                        }
                    }
                }

            }

        }
        return null;
    }

    public void run() throws Exception {
        Scanner sc = new Scanner(System.in);
        Player white = players[0];
        Player black = players[1];
        King myKing;
        while (true) {
            Player current;
            Player opponent;
            if (turn == 0) {
                current = white;
                opponent = black;
                myKing = (King) board.getPosition(board.whiteKing);
            } else {
                current = black;
                opponent = white;
                myKing = (King) board.getPosition(board.blackKing);

            }
            //before the turn actually start, reset all history to 0;
            current.promoType = null;
            current.twoSquareAdvanced = null;
            current.wantToDraw = false;
            //draw the board
            board.drawBoard();
            if (myKing.isChecked()) {
                if (isCheckMate(turn)) {
                    System.out.println("Checkmate");
                    System.out.println(opponent + " wins");
                    return;
                }
                System.out.println("Check");
                board.drawBoard();
            } else {
                if (isStaleMate(turn)) {
                    System.out.println("StaleMate");
                    System.out.println("Draw");
                    return;
                }
            }
            //neither checkMate Nor StaleMate
            //now ask for the user input
            while (true) {
                System.out.print(current + "'s move:");
                String input = sc.nextLine().trim().toLowerCase();
                //there can be only 5 types of input
                //length: 6-resign,5-move,7-move-and-promote,11-move-and-draw,4-draw;
                int length = input.length();
                if (length == 4) {
                    if (input.equals("draw")) {
                        if (opponent.wantToDraw) {
                            System.out.println("Draw");
                            return;
                        }
                    } else {
                        System.out.println("Illegal move, try again");
                    }
                } else if (length == 5) {
                    String position1 = input.substring(0, 2);
                    String position2 = input.substring(3);
                    Position parsedPosition1 = readPosition(position1);
                    Position parsedPosition2 = readPosition(position2);
                    if (parsedPosition1 == null || parsedPosition2 == null) {
                        System.out.println("Illegal move, try again");
                        continue;
                    }
                    Piece startPiece = board.getPosition(parsedPosition1);
                    if (startPiece == null) {
                        System.out.println("Illegal move, try again");
                        continue;
                    }
                    if (startPiece.isWhiteColor() != myKing.isWhiteColor()) {
                        System.out.println("Illegal move, try again");
                        continue;
                    }
                    if (startPiece.validateAndMove(parsedPosition1, parsedPosition2)) {
                        turn = 1 - turn;
                        break;
                    } else {
                        System.out.println("Illegal move, try again");
                        continue;
                    }
                } else if (length == 6) {
                    if (input.equals("resign")) {
                        System.out.println(opponent + " wins");
                        return;
                    }

                } else if (length == 7) {
                    String position1 = input.substring(0, 2);
                    String position2 = input.substring(3, 5);
                    Position parsedPosition1 = readPosition(position1);
                    Position parsedPosition2 = readPosition(position2);
                    switch (input.substring(6)) {
                        case "N":
                            current.promoType = Type.KNIGHT;
                            break;
                        case "R":
                            current.promoType = Type.ROOK;
                            break;
                        case "B":
                            current.promoType = Type.BISHOP;
                            break;
                        case "Q":
                        default:
                            current.promoType = Type.QUEEN;
                            break;
                    }
                    if (parsedPosition1 == null || parsedPosition2 == null) {
                        System.out.println("Illegal move, try again");
                        continue;
                    }
                    Piece startPiece = board.getPosition(parsedPosition1);
                    if (startPiece == null) {
                        System.out.println("Illegal move, try again");
                        continue;
                    }
                    if (startPiece.isWhiteColor() != myKing.isWhiteColor()) {
                        System.out.println("Illegal move, try again");
                        continue;
                    }
                    if (startPiece.validateAndMove(parsedPosition1, parsedPosition2)) {
                        turn = 1 - turn;
                        break;
                    } else {
                        System.out.println("Illegal move, try again");
                        continue;
                    }
                } else if (length == 11) {
                    String position1 = input.substring(0, 2);
                    String position2 = input.substring(3, 5);
                    String draw = input.substring(6);
                    if (draw.equals("draw?")) {
                        current.wantToDraw = true;
                    }
                    Position parsedPosition1 = readPosition(position1);
                    Position parsedPosition2 = readPosition(position2);
                    if (parsedPosition1 == null || parsedPosition2 == null) {
                        System.out.println("Illegal move, try again");
                        continue;
                    }
                    Piece startPiece = board.getPosition(parsedPosition1);
                    if (startPiece == null) {
                        System.out.println("Illegal move, try again");
                        continue;
                    }
                    if (startPiece.isWhiteColor() != myKing.isWhiteColor()) {
                        System.out.println("Illegal move, try again");
                        continue;
                    }
                    if (startPiece.validateAndMove(parsedPosition1, parsedPosition2)) {
                        turn = 1 - turn;
                        break;
                    } else {
                        System.out.println("Illegal move, try again");
                        continue;
                    }

                } else {
                    System.out.println("Illegal move, try again");
                }
            }
        }
    }

    public static Position readPosition(String input) {
        try {
            char first = input.charAt(0);
            int file = first - 'a';
            int rank = Integer.parseInt(input.substring(1)) - 1;
            if (file > -1 && file < 8 && rank > -1 && rank < 8) {
                return new Position(file, rank);
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }


}
