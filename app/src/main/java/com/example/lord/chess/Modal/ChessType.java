package com.example.lord.chess.Modal;

import java.io.Serializable;

public class ChessType implements Serializable {
    public enum Type {QUEEN, KNIGHT, ROOK, BISHOP, KING, PAWN}
}
