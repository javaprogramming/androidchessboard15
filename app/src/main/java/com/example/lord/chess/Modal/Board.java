package com.example.lord.chess.Modal;



import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.GridLayout;
import android.view.View;
import android.widget.ImageView;
import com.example.lord.chess.View.PlayGame;
import com.example.lord.chess.R;
import com.example.lord.chess.View.Playback;

import java.io.IOException;
import java.io.Serializable;

public class Board implements Serializable {
    int size = 8;
    Piece[][] table;
    private Player[] players;
    public Position whiteKing;
    public Position blackKing;
    Move move;
    public Position from;
    public Position to;

    public Board(Player[] players) {
        this.table = new Piece[size][size];
        this.players = players;
        initializeBoard();
        whiteKing = new Position(4, 0);
        blackKing = new Position(4, 7);
    }

    public Piece getPosition(int file, int rank) {
        return table[file][rank];

    }

    public Piece getPosition(Position position) {
        return table[position.getFile()][position.getRank()];

    }

    void setPosition(int file, int rank, Piece piece) {
        table[file][rank] = piece;
        if (piece != null) {
            piece.position = new Position(file, rank);
        }
    }

    void setPosition(Position position, Piece piece) {
        table[position.getFile()][position.getRank()] = piece;
        if (piece != null) {
            piece.position = position;
        }
    }

    public Position getWhiteKing() {
        return whiteKing;
    }

    public Position getBlackKing() {
        return blackKing;
    }

    private void initializeBoard() {
        table[0][0] = new Rook(players[0], new Position(0, 0), this);
        table[1][0] = new Knight(players[0], new Position(1, 0), this);
        table[2][0] = new Bishop(players[0], new Position(2, 0), this);
        table[3][0] = new Queen(players[0], new Position(3, 0), this);
        table[4][0] = new King(players[0], new Position(4, 0), this);
        table[5][0] = new Bishop(players[0], new Position(5, 0), this);
        table[6][0] = new Knight(players[0], new Position(6, 0), this);
        table[7][0] = new Rook(players[0], new Position(7, 0), this);
        table[0][1] = new Pawn(players[0], new Position(0, 1), this);
        table[1][1] = new Pawn(players[0], new Position(1, 1), this);
        table[2][1] = new Pawn(players[0], new Position(2, 1), this);
        table[3][1] = new Pawn(players[0], new Position(3, 1), this);
        table[4][1] = new Pawn(players[0], new Position(4, 1), this);
        table[5][1] = new Pawn(players[0], new Position(5, 1), this);
        table[6][1] = new Pawn(players[0], new Position(6, 1), this);
        table[7][1] = new Pawn(players[0], new Position(7, 1), this);
        table[0][6] = new Pawn(players[1], new Position(0, 6), this);
        table[1][6] = new Pawn(players[1], new Position(1, 6), this);
        table[2][6] = new Pawn(players[1], new Position(2, 6), this);
        table[3][6] = new Pawn(players[1], new Position(3, 6), this);
        table[4][6] = new Pawn(players[1], new Position(4, 6), this);
        table[5][6] = new Pawn(players[1], new Position(5, 6), this);
        table[6][6] = new Pawn(players[1], new Position(6, 6), this);
        table[7][6] = new Pawn(players[1], new Position(7, 6), this);
        table[0][7] = new Rook(players[1], new Position(0, 7), this);
        table[1][7] = new Knight(players[1], new Position(1, 7), this);
        table[2][7] = new Bishop(players[1], new Position(2, 7), this);
        table[3][7] = new Queen(players[1], new Position(3, 7), this);
        table[4][7] = new King(players[1], new Position(4, 7), this);
        table[5][7] = new Bishop(players[1], new Position(5, 7), this);
        table[6][7] = new Knight(players[1], new Position(6, 7), this);
        table[7][7] = new Rook(players[1], new Position(7, 7), this);
    }

    public void drawBoard() {
        int currRank = 8;
        for (int r = 7; r >= 0; r--) {
            for (int f = 0; f <= 7; f++) {
                if (table[f][r] == null) {
                    if ((f + r) % 2 == 0) {
                        System.out.print("## ");
                    }
                    else {
                        System.out.print("   ");
                    }
                } else {
                    System.out.print(table[f][r] + " ");
                }
            }
            System.out.println(currRank);
            currRank--;
        }
        System.out.println(" a  b  c  d  e  f  g  h  ");
    }
    public void paintBoard(GridLayout board,int turn,PlayGame game)
    {
        board.removeAllViewsInLayout();
        from = null;
        to = null;
        final PlayGame finalgame = game;
        for(int r = 7; r >= 0; r--) {
            for (int f = 0; f <= 7; f++) {
                final ImageView image;
                final int tmpf =f,tmpr =r;
                Bitmap bmp;
                int width=80;
                int height=80;
                int imagecode;
                int backgroundcode = -1;
                boolean willinitiate= false;
                boolean willend = true;
                if (table[f][r] == null) {
                    if ((f + r) % 2 == 0) {
                        imagecode = R.drawable.black;
                    }
                    else {
                        imagecode = R.drawable.white;
                    }
                }
                else {
                    if(table[f][r].isWhiteColor()&&turn == 0 || !table[f][r].isWhiteColor()&&turn==1)
                    {
                        willinitiate = true;
                        willend = false;
                    }
                    backgroundcode = R.drawable.white;
                    imagecode = table[f][r].icon;
                }
                final boolean initiate = willinitiate;
                final boolean end = willend;
                final int ignoreifnegative = backgroundcode;
                image = new ImageView(board.getContext());
                bmp= BitmapFactory.decodeResource(board.getResources(),imagecode);
                bmp=Bitmap.createScaledBitmap(bmp, width,height, true);
                if(backgroundcode!=-1)
                {
                    Bitmap background = BitmapFactory.decodeResource(board.getResources(),backgroundcode);
                    background = Bitmap.createScaledBitmap(background,width,height,true);
                    image.setBackground(new BitmapDrawable(board.getResources(),background));
                }
                image.setImageBitmap(bmp);
                image.setTag(new Position(f,r).toString());
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(from == null)
                        {
                            if(ignoreifnegative <0)
                            {
                                return;
                            }
                            if(initiate)
                            {
                                image.setBackgroundColor(Color.YELLOW);
                                from = new Position(tmpf,tmpr);
                            }
                        }
                        else
                        {
                            to = new Position(tmpf,tmpr);
                            if(from.equals(to))
                            {
                                from = null;
                                to = null;
                                image.setBackgroundColor(Color.WHITE);
                                return;
                            }
                            if(end)
                            {

                                move = new Move(from,to);
                                image.setBackgroundColor(Color.BLUE);
                                try {
                                    finalgame.inform(move);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                    }
                });

                board.addView(image);
            }
        }

    }
    public void simulateBoard(GridLayout board,int turn,Playback game)
    {
        board.removeAllViewsInLayout();
        from = null;
        to = null;
        final Playback finalgame = game;
        for(int r = 7; r >= 0; r--) {
            for (int f = 0; f <= 7; f++) {
                final ImageView image;
                final int tmpf =f,tmpr =r;
                Bitmap bmp;
                int width=80;
                int height=80;
                int imagecode;
                int backgroundcode = -1;
                if (table[f][r] == null) {
                    if ((f + r) % 2 == 0) {
                        imagecode = R.drawable.black;
                    }
                    else {
                        imagecode = R.drawable.white;
                    }
                }
                else {
                    backgroundcode = R.drawable.white;
                    imagecode = table[f][r].icon;
                }
                image = new ImageView(board.getContext());
                bmp= BitmapFactory.decodeResource(board.getResources(),imagecode);
                bmp=Bitmap.createScaledBitmap(bmp, width,height, true);
                if(backgroundcode!=-1)
                {
                    Bitmap background = BitmapFactory.decodeResource(board.getResources(),backgroundcode);
                    background = Bitmap.createScaledBitmap(background,width,height,true);
                    image.setBackground(new BitmapDrawable(board.getResources(),background));
                }
                image.setImageBitmap(bmp);
                image.setTag(new Position(f,r).toString());
                board.addView(image);
            }
        }

    }

}
