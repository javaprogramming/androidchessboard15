package com.example.lord.chess.View;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.lord.chess.Modal.Board;
import com.example.lord.chess.Modal.Game;
import com.example.lord.chess.Modal.Move;
import com.example.lord.chess.Modal.Piece;
import com.example.lord.chess.Modal.Player;
import com.example.lord.chess.Modal.SingleGame;
import com.example.lord.chess.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Dingren Nie on 12/14/2016.
 */

public class Playback extends AppCompatActivity {
    GridLayout chessboard;
    TextView textview;
    Game game;
    int turn = 0;
    int counter = 0;
    public Board copy;
    private ArrayList<Move> moves = new ArrayList<>();
    private ArrayList<SingleGame> games = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playback);
        textview = (TextView)findViewById(R.id.status);
        chessboard = (GridLayout) findViewById(R.id.replayboard);
        game = new Game();
        SingleGame replay = (SingleGame) getIntent().getSerializableExtra("game");
        moves = replay.move;
    }
    @Override
    protected void onResume() {
        super.onResume();
        initialize();
    }
    protected void initialize()
    {
        game.board.simulateBoard(chessboard,turn,this);
        textview.setText(game.players[turn]+"'s move");
    }

    public void next(View view){
        System.out.println(moves.size());
        if(counter == moves.size())
        {
            return;
        }
        else
        {
            Move move = moves.get(counter);
            Piece startPiece = game.board.getPosition(move.from);
            startPiece.validateAndMove(move.from, move.to);
            game.board.simulateBoard(chessboard,turn,this);
        }
        counter++;
        if(counter == moves.size())
        {
            final AlertDialog.Builder inputAlert = new AlertDialog.Builder(this);
            inputAlert.setTitle("Replay is done. ");
            final TextView userInput = new TextView(this);
            inputAlert.setView(userInput);
            inputAlert.setPositiveButton("Go back", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();

                    //readGame();
                }
            });
            AlertDialog alertDialog = inputAlert.create();
            alertDialog.show();
        }

    }





}
