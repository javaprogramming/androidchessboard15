package com.example.lord.chess.Modal;

import java.io.Serializable;

/**
 * Created by lord on 12/11/16.
 */

public class Move implements Serializable
{
    public Position from;
    public Position to;
    public Move(Position from, Position to)
    {
        this.from = from;
        this.to = to;
    }

}
