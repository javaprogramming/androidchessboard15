package com.example.lord.chess.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.example.lord.chess.Modal.DateComparator;
import com.example.lord.chess.Modal.SaveGame;
import com.example.lord.chess.Modal.SingleGame;
import com.example.lord.chess.Modal.TitleComparator;
import com.example.lord.chess.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.List;

import static com.example.lord.chess.R.layout.activity_playback;

public class Replay extends AppCompatActivity {
    ListView listView;
    ArrayAdapter<SingleGame> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_replay);
        listView = (ListView)findViewById(R.id.game_list);
        SaveGame game = new SaveGame();
        try{
            FileInputStream fi = openFileInput("savedGames");
            ObjectInputStream in = new ObjectInputStream(fi);
            game = (SaveGame) in .readObject();
            System.out.println("Game exist");
            System.out.println(game.games.size());
            System.out.print(game.games.get(0).date);
            in.close();
            fi.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        adapter = new ArrayAdapter<SingleGame>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, (List)game.games);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition     = position;

                // ListView Clicked item value
                SingleGame  item    = (SingleGame) listView.getItemAtPosition(position);

                // Show Alert
                Toast.makeText(getApplicationContext(),
                        "Position :"+itemPosition+"  ListItem : " +item , Toast.LENGTH_LONG)
                        .show();
                Intent returnBtn = new Intent(getApplicationContext(),
                        Playback.class);
                returnBtn.putExtra("game",item);

                startActivity(returnBtn);
            }

        });
    }
    public void clearall(View view)
    {
        File dir = getFilesDir();
        File file = new File(dir, "savedGames");

        if(file.delete())
        {
            System.out.println("delete Successful");
        }
        adapter.clear();
    }
    public void byDate(View view)
    {
        adapter.sort(new DateComparator());
    }
    public void byTitle(View view)
    {
        adapter.sort(new TitleComparator());
    }


}
