package com.example.lord.chess.Modal;

import java.io.Serializable;

public class Player implements Serializable {
    public boolean isWhite;
    //every round, want to Draw will be reinitialized for the player who is playing
    public boolean wantToDraw;
    public enPassant twoSquareAdvanced;//will be reset the beginning of the round
    public ChessType.Type promoType;//will get reset the beginning of the round

    public Player(boolean iswhite) {
        this.isWhite = iswhite;
        this.wantToDraw = false;
        this.promoType = null;
        this.twoSquareAdvanced = null;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        if (isWhite) {
            return "White";
        } else {
            return "Black";
        }
    }
}
