package com.example.lord.chess.Modal;

import java.util.Comparator;

/**
 * Created by lord on 12/15/16.
 */

public class TitleComparator implements Comparator<SingleGame> {

    @Override
    public int compare(SingleGame o1, SingleGame o2) {
        return o1.title.compareTo(o2.title);
    }
}
