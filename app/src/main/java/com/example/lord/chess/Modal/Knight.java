package com.example.lord.chess.Modal;

import java.util.ArrayList;
import java.util.List;

import com.example.lord.chess.Modal.ChessType.Type;
import com.example.lord.chess.R;

public class Knight extends Piece {
    public Knight(Player player, Position position, Board board) {
        this.player = player;
        this.position = position;
        this.board = board;
        this.type = Type.KNIGHT;
        if(player.isWhite)
        {
            icon = R.drawable.knight;
        }
        else
        {
            icon = R.drawable.knightfilled;
        }
    }

    @Override
    public List<Position> getRange(Board board) {
        // TODO Auto-generated method stub
        //idea is that we will check the eight spots that are available
        List<Position> result = new ArrayList<>();
        int file = this.position.getFile();
        int rank = this.position.getRank();
        //there will be eight possible positions that we can move to,
        //	and should be checked one by another
        //they are  ---note I made it (rank,file)
        //(rank-2,file-1)/(rank-2,file+1)
        //(rank-1,file-2)/(rank-1,file+2)
        //(rank+1,file-2)/(rank+1,file+2)
        //(rank+2,file-1)/(rank+2,file+1)
        if (isValidPosition(file - 1, rank - 2, this.isWhiteColor())) {
            result.add(new Position(file - 1, rank - 2));
        }
        if (isValidPosition(file + 1, rank - 2, this.isWhiteColor())) {
            result.add(new Position(file + 1, rank - 2));
        }
        if (isValidPosition(file - 2, rank - 1, this.isWhiteColor())) {
            result.add(new Position(file - 2, rank - 1));
        }
        if (isValidPosition(file + 2, rank - 1, this.isWhiteColor())) {
            result.add(new Position(file + 2, rank - 1));
        }
        if (isValidPosition(file - 2, rank + 1, this.isWhiteColor())) {
            result.add(new Position(file - 2, rank + 1));
        }
        if (isValidPosition(file + 2, rank + 1, this.isWhiteColor())) {
            result.add(new Position(file + 2, rank + 1));
        }
        if (isValidPosition(file - 1, rank + 2, this.isWhiteColor())) {
            result.add(new Position(file - 1, rank + 2));
        }
        if (isValidPosition(file + 1, rank + 2, this.isWhiteColor())) {
            result.add(new Position(file + 1, rank + 2));
        }
        return result;
    }


}
