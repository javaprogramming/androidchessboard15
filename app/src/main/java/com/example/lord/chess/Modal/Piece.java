package com.example.lord.chess.Modal;

import com.example.lord.chess.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class Piece implements Serializable {
    protected Player player;
    protected Position position;
    public Board board;
    protected int icon;
    protected ChessType.Type type;

    public boolean isWhiteColor() {
        return player.isWhite;
    }

    //notice these param of positions are passing by the game
    //this will work for Bishop, Queen, Knight
    public boolean validateAndMove(Position oldP, Position newP) {
        King myKing;
        if (this.isWhiteColor()) {
            myKing = (King) this.board.getPosition(this.board.whiteKing);
        } else {
            myKing = (King) this.board.getPosition(this.board.blackKing);
        }
        Piece mightCaptured = this.board.getPosition(newP);
        Piece mover = this;
        if (mover.getRange(this.board) != null && mover.getRange(this.board).contains(newP)) {//simulate the move
            this.board.setPosition(newP, mover);
            this.board.setPosition(oldP, null);
            if (myKing.isChecked()) {//revert the move
                this.board.setPosition(newP, mightCaptured);
                this.board.setPosition(oldP, mover);
                return false;
            }
            //other wise, clean up mightCaptured
            mightCaptured = null;
            return true;
        }
        //cannot go to the new position, then nothing has been changed, simply return false
        return false;
    }

    ;

    protected boolean validateOnly(Position oldP, Position newP) {
        King myKing;
        if (this.isWhiteColor()) {
            myKing = (King) this.board.getPosition(this.board.whiteKing);
        } else {
            myKing = (King) this.board.getPosition(this.board.blackKing);
        }
        Piece mightCaptured = this.board.getPosition(newP);
        Piece mover = this;
        if (mover.getRange(this.board) != null && mover.getRange(this.board).contains(newP)) {//simulate the move
            this.board.setPosition(newP, mover);
            this.board.setPosition(oldP, null);
            if (myKing.isChecked()) {//revert the move
                this.board.setPosition(newP, mightCaptured);
                this.board.setPosition(oldP, mover);
                return false;
            }
            //other wise, clean up mightCaptured
            this.board.setPosition(newP, mightCaptured);
            this.board.setPosition(oldP, mover);
            return true;
        }
        //cannot go to the new position, then nothing has been changed, simply return false
        return false;
    }

    ;

    public abstract List<Position> getRange(Board board);

    //the following method make it less efficient, but will make our programming much more efficient
    protected boolean isValidPosition(int file, int rank, boolean callerIsWhite) {    //in the right range
        if ((rank > -1) && (rank < 8) && (file > -1) && (file < 8)) {
            Piece tmp = board.getPosition(file, rank);
            if (tmp == null)//it is empty
            {
                return true;
            } else if (tmp.isWhiteColor() != callerIsWhite)//not the same color
            {
                return true;
            }
            return false;
        }
        return false;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        String result = "";
        if (this.isWhiteColor()) {
            result += "w";
        } else {
            result += "b";
        }
        switch (this.type) {
            case BISHOP:
                result += "B";
                break;
            case KING:
                result += "K";
                break;
            case KNIGHT:
                result += "N";
                break;
            case PAWN:
                result += "p";
                break;
            case QUEEN:
                result += "Q";
                break;
            case ROOK:
                result += "R";
                break;
        }
        return result;
    }
}
