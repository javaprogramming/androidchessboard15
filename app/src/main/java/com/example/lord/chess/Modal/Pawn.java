package com.example.lord.chess.Modal;

import java.util.ArrayList;
import java.util.List;

import com.example.lord.chess.Modal.ChessType.Type;
import com.example.lord.chess.R;

public class Pawn extends Piece {
    private boolean hasMoved;

    public Pawn(Player player, Position position, Board board) {
        this.player = player;
        this.position = position;
        this.board = board;
        this.hasMoved = false;
        this.type = Type.PAWN;
        if(player.isWhite)
        {
            icon = R.drawable.pawn;
        }
        else
        {
            icon = R.drawable.pawnfilled;
        }
    }

    public boolean isMoved() {
        return hasMoved;
    }

    @Override
    public List<Position> getRange(Board board) {
        try {
            // TODO Auto-generated method stub
            //at this point we will check if the Pawn has moved
            List<Position> result = new ArrayList<>();
            int file = this.position.getFile();
            int rank = this.position.getRank();
            if (this.isWhiteColor()) {
                //(file-1,rank+1),(file+1,rank+1) notice this two might result in enPassant
                //(file,rank+1)
                //(file,rank+2)
                if (isValidPosition(file - 1, rank + 1, true)) {
                    Position toCheck = new Position(file - 1, rank + 1);
                    if (this.board.getPosition(file - 1, rank + 1) != null)//not an empty space
                    {
                        result.add(toCheck);
                    } else //this one is empty, so the only possibility is enPassant
                    {
                        Position black = this.board.blackKing;
                        Player opponent = this.board.getPosition(black).player;
                        if (opponent.twoSquareAdvanced != null)//has moved 2-square advance in last move
                        {
                            if (opponent.twoSquareAdvanced.tombstone.equals(toCheck)) {
                                result.add(toCheck);
                            }
                            //else ignore
                        }
                    }
                }
                if (isValidPosition(file + 1, rank + 1, true)) {
                    Position toCheck = new Position(file + 1, rank + 1);
                    if (this.board.getPosition(toCheck) != null)//can be taken over
                    {
                        result.add(toCheck);
                    } else //this one is empty, so the only possibility is enPassant
                    {
                        Position black = this.board.blackKing;
                        Player opponent = this.board.getPosition(black).player;
                        if (opponent.twoSquareAdvanced != null)//has moved 2-square advance in last move
                        {
                            if (opponent.twoSquareAdvanced.tombstone.equals(toCheck)) {
                                result.add(toCheck);
                            }
                            //else ignore
                        }
                    }
                }
                if (isValidPosition(file, rank + 1, true) && this.board.getPosition(file, rank + 1) == null) {
                    result.add(new Position(file, rank + 1));
                }
                if (!this.hasMoved) {
                    if (this.board.getPosition(file, rank + 1) == null
                            && this.board.getPosition(file, rank + 2) == null) {
                        result.add(new Position(file, rank + 2));
                    }
                }
            } else//color is black
            {
                //(file-1,rank-1),(file+1,rank-1) notice this two might result in enPassant
                //(file,rank-1)
                //(file,rank-2)
                if (isValidPosition(file - 1, rank - 1, false)) {
                    Position toCheck = new Position(file - 1, rank - 1);
                    if (this.board.getPosition(toCheck) != null)//can be taken over
                    {
                        result.add(toCheck);
                    } else //this one is empty, so the only possibility is enPassant
                    {
                        Position white = this.board.whiteKing;
                        Player opponent = this.board.getPosition(white).player;
                        if (opponent.twoSquareAdvanced != null)//has moved 2-square advance in last move
                        {
                            if (opponent.twoSquareAdvanced.tombstone.equals(toCheck)) {
                                result.add(toCheck);
                            }
                            //else ignore
                        }
                    }
                }
                if (isValidPosition(file + 1, rank - 1, false)) {
                    Position toCheck = new Position(file + 1, rank - 1);
                    if (this.board.getPosition(toCheck) != null)//can be taken over
                    {
                        result.add(toCheck);
                    } else //this one is empty, so the only possibility is enPassant
                    {
                        Position white = this.board.whiteKing;
                        Player opponent = this.board.getPosition(white).player;
                        if (opponent.twoSquareAdvanced != null)//has moved 2-square advance in last move
                        {
                            if (opponent.twoSquareAdvanced.tombstone.equals(toCheck)) {
                                result.add(toCheck);
                            }
                            //else ignore
                        }
                    }
                }
                if (isValidPosition(file, rank - 1, false) && this.board.getPosition(file, rank - 1) == null)//there is no enemy
                {
                    result.add(new Position(file, rank - 1));
                }
                if (!this.hasMoved) {
                    if (this.board.getPosition(file, rank - 1) == null
                            && this.board.getPosition(file, rank - 2) == null) {
                        result.add(new Position(file, rank - 2));
                    }
                }

            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean validateAndMove(Position oldP, Position newP) {
        // TODO Auto-generated method stub
        //after everything, if we moved, we will check for possible promotion
        King myKing;
        Player opponent;
        int promoteRank;
        if (this.isWhiteColor()) {
            promoteRank = 7;
            myKing = (King) this.board.getPosition(this.board.whiteKing);
            opponent = this.board.getPosition(this.board.blackKing).player;
        } else {
            promoteRank = 0;
            myKing = (King) this.board.getPosition(this.board.blackKing);
            opponent = this.board.getPosition(this.board.whiteKing).player;
        }
        Piece mightCaptured = this.board.getPosition(newP);
        Piece mover = this;
        if (mover.getRange(this.board) != null && mover.getRange(this.board).contains(newP)) {
            //before we simulate the moves, we have to make sure,cases where target
            //position is empty and it is not emnPassant, and its on different file will be excluded
            if (newP.getFile() != mover.position.getFile() && (this.board.getPosition(newP) == null)) {
                //enPassant matches
                Piece getkilled = this.board.getPosition(opponent.twoSquareAdvanced.mostRecent);
                this.board.setPosition(newP, mover);
                this.board.setPosition(oldP, null);
                this.board.setPosition(opponent.twoSquareAdvanced.mostRecent, null);
                if (myKing.isChecked()) {//revert the move
                    this.board.setPosition(opponent.twoSquareAdvanced.mostRecent, getkilled);
                    this.board.setPosition(newP, null);
                    this.board.setPosition(oldP, mover);
                    return false;
                }
                getkilled = null;    //release object
            } else {
                //simulate the move
                this.board.setPosition(newP, mover);
                this.board.setPosition(oldP, null);
                if (myKing.isChecked()) {//revert the move
                    this.board.setPosition(newP, mightCaptured);
                    this.board.setPosition(oldP, mover);
                    return false;
                }
                //other wise, clean up mightCaptured
                mightCaptured = null;
                //since now we succeed, will check if it was a two-square-advance
                if ((oldP.getFile() == newP.getFile()) && (oldP.getDistance(newP) == 2)) {
                    //it is a two square advance
                    myKing.player.twoSquareAdvanced = new enPassant
                            (newP, new Position(oldP.getFile(), (oldP.getRank() + newP.getRank()) / 2));
                }
            }
            //now has moved will set to true and will check for promote
            this.hasMoved = true;
            if (this.position.getRank() == promoteRank) {
                if (myKing.player.promoType == null) {
                    myKing.player.promoType = Type.QUEEN;
                }
                switch (myKing.player.promoType) {
                    case BISHOP:
                        this.board.setPosition(this.position, new Bishop(myKing.player, this.position, this.board));
                        break;
                    case KNIGHT:
                        this.board.setPosition(this.position, new Knight(myKing.player, this.position, this.board));
                        break;
                    case ROOK:
                        this.board.setPosition(this.position, new Rook(myKing.player, this.position, this.board));
                        break;
                    case QUEEN:
                    default:
                        this.board.setPosition(this.position, new Queen(myKing.player, this.position, this.board));
                        break;
                }
            }
            return true;
        }
        return false;
    }

    @Override
    protected boolean validateOnly(Position oldP, Position newP) {
        // TODO Auto-generated method stub
        //after everything, if we moved, we will check for possible promotion
        King myKing;
        Player opponent;
        int promoteRank;
        if (this.isWhiteColor()) {
            promoteRank = 7;
            myKing = (King) this.board.getPosition(this.board.whiteKing);
            opponent = this.board.getPosition(this.board.blackKing).player;
        } else {
            promoteRank = 0;
            myKing = (King) this.board.getPosition(this.board.blackKing);
            opponent = this.board.getPosition(this.board.whiteKing).player;
        }
        Piece mightCaptured = this.board.getPosition(newP);
        Piece mover = this;
        if (mover.getRange(this.board) != null && mover.getRange(this.board).contains(newP)) {
            //before we simulate the moves, we have to make sure,cases where target
            //position is empty and it is not emnPassant, and its on different file will be excluded
            if (newP.getFile() != mover.position.getFile() && (this.board.getPosition(newP) == null)) {
                //enPassant matches
                Piece getkilled = this.board.getPosition(opponent.twoSquareAdvanced.mostRecent);
                this.board.setPosition(newP, mover);
                this.board.setPosition(oldP, null);
                this.board.setPosition(opponent.twoSquareAdvanced.mostRecent, null);
                if (myKing.isChecked()) {//revert the move
                    this.board.setPosition(opponent.twoSquareAdvanced.mostRecent, getkilled);
                    this.board.setPosition(newP, null);
                    this.board.setPosition(oldP, mover);
                    return false;
                }//succeed
                this.board.setPosition(opponent.twoSquareAdvanced.mostRecent, getkilled);
                this.board.setPosition(newP, null);
                this.board.setPosition(oldP, mover);
            } else {
                //simulate the move
                this.board.setPosition(newP, mover);
                this.board.setPosition(oldP, null);
                if (myKing.isChecked()) {//revert the move
                    this.board.setPosition(newP, mightCaptured);
                    this.board.setPosition(oldP, mover);
                    return false;
                }
                //succeed
            }
            this.board.setPosition(newP, mightCaptured);
            this.board.setPosition(oldP, mover);
            return true;
        }
        return false;
    }

}
