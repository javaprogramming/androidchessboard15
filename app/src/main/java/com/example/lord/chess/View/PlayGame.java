package com.example.lord.chess.View;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayout;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.Toast;


import com.example.lord.chess.Modal.Board;
import com.example.lord.chess.Modal.Game;
import com.example.lord.chess.Modal.King;
import com.example.lord.chess.Modal.Move;
import com.example.lord.chess.Modal.Piece;
import com.example.lord.chess.Modal.Player;
import com.example.lord.chess.Modal.Position;
import com.example.lord.chess.Modal.SaveGame;
import com.example.lord.chess.Modal.SingleGame;
import com.example.lord.chess.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;

import static android.R.transition.move;
import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;
import static com.example.lord.chess.R.id.game_list;

public class PlayGame extends AppCompatActivity {
    GridLayout chessboard;
    TextView textview;
    Game game;
    Player white;
    Player black;
    int turn = 0;
    private ArrayList<Move> moves = new ArrayList<>();
    private SaveGame games;
    private Game copy;
    Button undo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_game);
        textview = (TextView)findViewById(R.id.instruction);
        chessboard = (GridLayout) findViewById(R.id.board);
        game = new Game();
        undo = (Button) findViewById(R.id.undo);
        Player white = game.players[0];
        Player black = game.players[1];
    }

    @Override
    protected void onResume() {
        super.onResume();
        initialize();
    }
    protected void initialize()
    {
        game.board.paintBoard(chessboard,turn,this);
        textview.setText(game.players[turn]+"'s move");
    }
    public void inform(Move move)  throws IOException{
        FileOutputStream fos = openFileOutput("tmp", Context.MODE_PRIVATE);
        ObjectOutputStream output= new ObjectOutputStream(fos);
        output.writeObject(game);
        fos.close();
        Piece startPiece = game.board.getPosition(move.from);
        System.out.println(move.from);
        System.out.println(move.to);
        King myKing;
        King opponentKing;

        if (startPiece.validateAndMove(move.from, move.to)) {
            //we know that the last move was valid
            //now check if we should finish the        disableUndo = true; game(checkMate, Stalemate)
            //Add the Code here
            undo.setEnabled(true);
            moves.add(move); //remember the move
            System.out.println(moves.size());
            if (turn == 0) {

                myKing = (King) game.board.getPosition(game.board.whiteKing);
                opponentKing = (King) game.board.getPosition(game.board.blackKing);
                //System.out.print(myKing.isChecked());
            } else {

                myKing = (King) game.board.getPosition(game.board.blackKing);
                opponentKing = (King) game.board.getPosition(game.board.whiteKing);
                //System.out.print(myKing.isChecked());
            }
//            current.promoType = null;
//            current.twoSquareAdvanced = null;
            if (opponentKing.isChecked()) {
                if (game.isCheckMate(1-turn)) {
                    textview.setText("Checkmate, "+game.players[turn]+" win");
                    Toast.makeText(PlayGame.this,
                            game.players[1-turn]+" win", Toast.LENGTH_LONG).show();
                    //save
                    final AlertDialog.Builder inputAlert = new AlertDialog.Builder(this);
                    inputAlert.setTitle("Title of the Game");
                    inputAlert.setMessage("Give the game a name to save");
                    final EditText userInput = new EditText(this);
                    inputAlert.setView(userInput);
                    inputAlert.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String userInputValue = userInput.getText().toString();
                            Date date = new Date();
                            SingleGame s = new SingleGame(date,moves,userInputValue);

                            System.out.println("####"+ s.getTitle()+s.getDate()+s.getMoves());
                            try {
                                saveGame(s);
                                finish();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            //readGame();
                        }
                    });
                    inputAlert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    AlertDialog alertDialog = inputAlert.create();
                    alertDialog.show();
                    return;
                }
                else
                {
                    turn = 1 - turn;
                    textview.setText(game.players[turn]+"'s checked");
                    game.board.paintBoard(chessboard,turn,this);
                    return;
                }
            } else {
                if (game.isStaleMate(1-turn)) {
                    textview.setText("Stalemate, draw");
                    Toast.makeText(PlayGame.this,
                            "Game Draws", Toast.LENGTH_LONG).show();
                    //save
                    final AlertDialog.Builder inputAlert = new AlertDialog.Builder(this);
                    inputAlert.setTitle("Title of the Game");
                    inputAlert.setMessage("Give the game a name to save");
                    final EditText userInput = new EditText(this);
                    inputAlert.setView(userInput);
                    inputAlert.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String userInputValue = userInput.getText().toString();
                            System.out.println(userInputValue);
                            Date date = new Date();
                            SingleGame s = new SingleGame(date,moves,userInputValue);

                            System.out.println("####"+ s.getTitle()+s.getDate()+s.getMoves());
                            try {
                                saveGame(s);
                                finish();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            //readGame();
                        }
                    });
                    inputAlert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    AlertDialog alertDialog = inputAlert.create();
                    alertDialog.show();
                    return;
                }
            }
            // End code
            turn = 1 - turn;
            game.board.paintBoard(chessboard,turn,this);
            //check if there is a check mate

            //if not
            textview.setText(game.players[turn] + "'s move");
                //if yes
            //set text to whose turn + is checked

        }
        else
        {
            Toast.makeText(PlayGame.this,
                    "invalid move", Toast.LENGTH_LONG).show();
            game.board.paintBoard(chessboard,turn,this);
            //show a toast that this is a invalid operation, duration set to Long
            //then reset the board(repaint the board)
            //do not change the turn
            //
        }


    }

    public void draw(View view){
        textview.setText("Game Draws.");

        Toast.makeText(PlayGame.this,
                "Game Draws", Toast.LENGTH_LONG).show();
        final AlertDialog.Builder inputAlert = new AlertDialog.Builder(this);
        inputAlert.setTitle("Title of the Game");
        inputAlert.setMessage("Give the game a name to save");
        final EditText userInput = new EditText(this);
        inputAlert.setView(userInput);
        inputAlert.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String userInputValue = userInput.getText().toString();
                if(userInputValue.trim().isEmpty())
                {
                    userInputValue = "Not_named";
                }
                System.out.println(userInputValue);
                Date date = new Date();
                System.out.println("draw: "+moves.size());
                SingleGame s = new SingleGame(date,moves,userInputValue);
                try {
                    saveGame(s);
                    finish();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
        inputAlert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        AlertDialog alertDialog = inputAlert.create();
        alertDialog.show();
        //this will return mainActivity without clicking save buttion
        /* Intent returnBtn = new Intent(getApplicationContext(),
                MainActivity.class);

        startActivity(returnBtn);*/


    }

    public void resign(View view){
        turn = 1 - turn;
        textview.setText(game.players[turn]+" win");
        Toast.makeText(PlayGame.this,
                game.players[turn]+" win", Toast.LENGTH_LONG).show();
        final AlertDialog.Builder inputAlert = new AlertDialog.Builder(this);
        inputAlert.setTitle("Title of the Game");
        inputAlert.setMessage("Give the game a name to save");
        final EditText userInput = new EditText(this);
        inputAlert.setView(userInput);
        inputAlert.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String userInputValue = userInput.getText().toString();
                if(userInputValue.trim().isEmpty())
                {
                    userInputValue = "Not_named";
                }
                Date date = new Date();
                SingleGame s = new SingleGame(date,moves,userInputValue);
                try {
                    saveGame(s);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //readGame();
            }
        });
        inputAlert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        AlertDialog alertDialog = inputAlert.create();
        alertDialog.show();

        //this will return mainActivity without clicking save buttion
        /* Intent returnBtn = new Intent(getApplicationContext(),
                MainActivity.class);

        startActivity(returnBtn);*/
    }
    private void saveGame(SingleGame s) throws IOException {
        games = readGame();
        games.add(s);
        FileOutputStream fos = null;
        try {
            fos = openFileOutput("savedGames", Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(games);
            System.out.println("save successfully");
            fos.close();
            finish();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    public void undo(View view) throws IOException, ClassNotFoundException {
        FileInputStream fi = openFileInput("tmp");
        ObjectInputStream in = new ObjectInputStream(fi);
        game = (Game) in .readObject();
        turn = 1-turn;
        textview.setText(game.players[turn]+"'s move");
        if(moves.size() == 0)
        {
            return;
        }
        moves.remove(moves.size()-1);
        undo.setEnabled(false);

        game.board.paintBoard(chessboard,turn,this);

    }

    public SaveGame readGame(){
        SaveGame game = new SaveGame();
        try{
            FileInputStream fi = openFileInput("savedGames");
            ObjectInputStream in = new ObjectInputStream(fi);
            game = (SaveGame) in .readObject();
            System.out.println("Game exist");
            System.out.println(game.games.size());
            System.out.print(game.games.get(0).date);
            in.close();
            fi.close();
            return game;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return game;
    }
    public void ai(View view) throws IOException
    {
        King king;
        if (turn == 0) {
            king = (King) game.board.getPosition(game.board.whiteKing);
        } else {
            king = (King) game.board.getPosition(game.board.blackKing);
        }
        inform(game.AIMove(king));
    }

}
