package com.example.lord.chess.Modal;

import java.io.Serializable;

public class Position implements Serializable{
    private int file;
    private int rank;

    public Position(int file, int rank) {
        this.file = file;
        this.rank = rank;
    }

    public int getFile() {
        return file;
    }

    public int getRank() {
        return rank;
    }

    @Override
    public boolean equals(Object obj) {
        // TODO Auto-generated method stub
        Position target = (Position) obj;
        return this.file == target.getFile() && this.rank == target.getRank();
    }

    public int getDistance(Position another) {
        return Math.abs(this.file - another.file) + Math.abs(this.rank - another.rank);
    }

    @Override
    public String toString() {
        return "file: "+file+", rank: "+rank;
    }
}
